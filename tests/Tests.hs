module Main where

import           Test.Tasty

import           Robot.Tests.Bool
import           Robot.Tests.Int
import           Robot.Tests.Instructions
import           Robot.Tests.MachineSimple
import           Robot.Tests.MachineFun
import           Robot.Tests.MachineCallcc
import           Robot.Tests.MachineObject
import           Robot.Tests.MachineExcept
import           Robot.Tests.MachineIO

main :: IO ()
main = defaultMain $ testGroup "Tests" [ evalBool
                                       , evalInt
                                       , transInstr
                                       , machineSimple
                                       , machineFun
                                       , machineCallcc
                                       , machineObject
                                       , machineExcept
                                       , machineIO
                                       ]
