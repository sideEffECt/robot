module Robot.Programs where

import           Robot.Lang


expressionTrue :: Lang
expressionTrue = LIf (LOr [LFalse, LTrue])
                        (LNot (LNot (LNot (LAnd [LTrue, LFalse]))))
                        (LIf LFalse LFalse LFalse)


simpleFunTrue :: Lang
simpleFunTrue = LApp (LLambda ["x", "y", "z"]
                         (LAnd [LVar "x", LVar "y", LVar "z"]))
                     [LTrue, LTrue, LTrue]


multiuseFunTrue :: Lang
multiuseFunTrue = LApp (LLambda ["orrer"]
                          (LOr [LApp (LVar "orrer") [LFalse],
                                 LApp (LVar "orrer") [LTrue]]))
                       [LLambda ["x"] (LOr [LVar "x", LFalse])]


multiuseFun2True :: Lang
multiuseFun2True = LApp
                      (LLambda ["orrer"]
                        (LApp
                          (LLambda ["orfalse"]
                            (LApp (LVar "orfalse") [LTrue]))
                          [LApp (LVar "orrer") [LFalse]]
                        )
                      )
                      [LLambda ["x", "y"] (LOr [LVar "x", LVar "y"])]


multiuseFun3False :: Lang
multiuseFun3False = LApp
                      (LLambda ["orrer"]
                        (LApp (LVar "orrer") [LFalse])
                      )
                      [LLambda ["x"] (LOr [LVar "x", LFalse])]



--------------------------------------------------


setTrue :: Lang
setTrue = LApp
               (LLambda ["x"]
                 (LSeqnc [LSet "x" LTrue,
                         LVar "x"]
                 )
               )
               [LUnit]


funs0 :: Lang
funs0 = LLetRec [("f", LLambda ["n"]
                     (LApp (LVar "g") [LVar "n"]))
                ,("g", LLambda ["n"]
                     (LVar "n"))
                ]
                (LApp (LVar "f") [LInt 0])


rec0 :: Lang
rec0 = LLetRec [("rec", LLambda ["n"]
                    (LIf (LEq [LVar "n", LInt 0])
                         (LInt 0)
                         (LApp (LVar "rec") [LMinus [LVar "n", LInt 1]])
                    )
               )]
               (LApp (LVar "rec") [LInt 3])


factorial :: (Identifier, Lang)
factorial = ("fac", LLambda ["n"]
                               (LIf (LLe [LVar "n", LInt 1])
                                    (LInt 1)
                                    (LMul [ LVar "n",
                                            LApp (LVar "fac") [LMinus [LVar "n", LInt 1]]
                                          ]
                                    )
                               )
                           )


fac24 :: Lang
fac24 = LLetRec [factorial]
                (LApp (LVar "fac") [LInt 4])


adder12 :: Lang
adder12 = LLet [("make-adder", LLambda ["x"]
                  (LLambda ["y"] (LPlus [LVar "x", LVar "y"]))),
                ("add5", LApp (LVar "make-adder") [LInt 5])
               ]
               (LApp (LVar "add5") [LInt 7])


adderRec12 :: Lang
adderRec12 = LLetRec [("make-adder", LLambda ["x"]
                       (LLambda ["y"] (LPlus [LVar "x", LVar "y"]))),
                      ("add5", LApp (LVar "make-adder") [LInt 5])
                     ]
                     (LApp (LVar "add5") [LInt 7])


subber2 :: Lang
subber2 = LLetRec [("make-subber", LLambda ["x", "y"]
                     (LMinus [LVar "y", LVar "x"])),
                   ("sub5", LApp (LVar "make-subber") [LInt 5])
                  ]
                  (LApp (LVar "sub5") [LInt 7])


-----------------------------------------------------


appLambda :: Lang
appLambda = LApp (LLambda ["x"] LTrue) [LFalse]

simpleIf :: Lang
simpleIf = LIf LTrue LFalse LTrue


------------------------------------------------------


callccNull3 :: Lang
callccNull3 = LLet [("f", LLambda ["return"]
                          (LSeqnc [LApp (LVar "return") [LInt 2],
                                   LInt 3
                                  ]
                          )
                    )
                   ]
                   (LApp (LVar "f") [LLambda ["x"] (LVar "x")])


callcc2 :: Lang
callcc2 = LLet [("f", LLambda ["return"]
                      (LSeqnc [LApp (LVar "return") [LInt 2],
                               LInt 3
                              ]
                      )
                )
               ]
               (LCallCC (LVar "f"))


---------------------------------------------------------


objectSimpleTrue :: Lang
objectSimpleTrue = LLet [("Object", LObject Nothing [("getTrue", LTrue)
                                                    ])
                        ]
                        (LInvoke (LVar "Object") "getTrue" [])


objectNegateFalse :: Lang
objectNegateFalse = LLet [("Object", LObject Nothing [("negate", LLambda ["x"]
                                                         (LNot (LVar "x")))
                                                     ])
                         ]
                         (LInvoke (LVar "Object") "negate" [LTrue])


objectSub2 :: Lang
objectSub2 = LLet [("Object", LObject Nothing [("minus", LLambda ["x", "y"]
                                                  (LMinus [LVar "x", LVar "y"]))
                                              ])
                  ]
                  (LInvoke (LVar "Object") "minus" [LInt 7, LInt 5])


objectMoreMethodsTrue :: Lang
objectMoreMethodsTrue = LLet [("Object", LObject Nothing [("getTrue", LTrue)
                                                         ,("getTrue2", LInvoke (LVar "this") "getTrue" [])
                                                         ])
                             ]
                             (LInvoke (LVar "Object") "getTrue2" [])


objectMoreMethods4 :: Lang
objectMoreMethods4 = LLet [("Object", LObject Nothing [("minus", LLambda ["x", "y"]
                                                          (LMinus [LVar "x", LVar "y"]))
                                                      ,("xxx", LLambda ["x", "y"]
                                                          (LMul [LInt 2, LInvoke (LVar "this") "minus" [LVar "x", LVar "y"]]))
                                                      ]
                          )]
                          (LInvoke (LVar "Object") "xxx" [LInt 7, LInt 5])


objectInheritance13 :: Lang
objectInheritance13 = LLet [("Parent", LObject Nothing [("a",
                                                           LInt 7)
                                                       ,("b",
                                                           LInvoke (LVar "this") "a" [])
                                                       ])
                           ,("Child", LObject (Just (LVar "Parent")) [("a",
                                                                         LInt 13)
                                                                     ])
                           ]
                           (LInvoke (LVar "Child") "b" [])


objectInheritance99 :: Lang
objectInheritance99 = LLet [("Parent", LObject Nothing [("a",
                                                           LInt 7)
                                                       ,("b",
                                                           LInvoke (LVar "this") "a" [])
                                                       ])
                           ,("getParent", LLambda ["x"]
                                          (LIf (LEq [LPlus [LInt 1, LInt 1], LVar "x"])
                                               (LVar "Parent")
                                               LUnit))
                           ,("Child", LObject (Just (LApp (LVar "getParent") [LInt 2])) [("a",
                                                                                            LInt 99)
                                                                                        ])
                           ]
                           (LInvoke (LVar "Child") "b" [])


-------------------------------------------------------------------------


exceptSimple3 :: Lang
exceptSimple3 = LTryCatch (LThrow (LInt 3)) "ex" (LVar "ex")


exceptNull7 :: Lang
exceptNull7 = LTryCatch (LIf LFalse
                             (LThrow (LInt 9))
                             (LInt 7))
                        "ex"
                        (LVar "ex")


exceptSimple9 :: Lang
exceptSimple9 = LTryCatch (LIf LTrue
                               (LThrow (LInt 9))
                               (LInt 7))
                          "ex"
                          (LVar "ex")


exceptSeqnc9 :: Lang
exceptSeqnc9 = LTryCatch (LSeqnc [LIf LTrue
                                      (LThrow (LInt 9))
                                      (LInt 7)
                                 ,LInt 4
                                 ])
                         "ex"
                         (LVar "ex")


exceptSeqnc4 :: Lang
exceptSeqnc4 = LTryCatch (LSeqnc [LIf LFalse
                                      (LThrow (LInt 9))
                                      (LInt 7)
                                 ,LInt 4
                                 ])
                         "ex"
                         (LVar "ex")


--------------------------------------------------------------------------


ioReadSimple :: Lang
ioReadSimple = LMinus [LRead, LRead]


ioWriteSimple :: Lang
ioWriteSimple = LSeqnc [LWrite (LInt 7), LWrite (LInt 13)]



ioFac :: Lang
ioFac = LLetRec [factorial]
                (LWrite (LApp (LVar "fac") [LRead]))


ioFacSeries :: Lang
ioFacSeries = LLetRec [ ("inputsize", LRead)
                      , factorial
                      , ("loop", LLambda ["counter"]
                                         (LIf (LLe [LVar "counter", LInt 0])
                                             LUnit
                                             (LSeqnc [ LWrite (LApp (LVar "fac") [LRead])
                                                     , LApp (LVar "loop") [LMinus [LVar "counter", LInt 1]]
                                                     ]
                                             )
                                         )
                        )
                      ]
                      (LApp (LVar "loop") [LVar "inputsize"])

