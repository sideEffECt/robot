module Robot.Tests.MachineObject where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineObject :: TestTree
machineObject = testGroup "Call-with-current-continuation"
    [ testCase     "simple object method call"                  testObjectSimpleTrue
    , testCase     "method with parameter"                      testObjectNegateFalse
    , testCase     "method with parameters"                     testObjectSub2
    , testCase     "transitive method calling"                  testObjectMoreMethodsTrue
    , testCase     "transitive method calling with parameters"  testObjectMoreMethods4
    , testCase     "inheritance"                                testObjectInheritance13
    , testCase     "computing inheritance"                      testObjectInheritance99
    ]

testObjectSimpleTrue :: Assertion
testObjectSimpleTrue = VTrue @=? machine objectSimpleTrue

testObjectNegateFalse :: Assertion
testObjectNegateFalse = VFalse @=? machine objectNegateFalse

testObjectSub2 :: Assertion
testObjectSub2 = VInt 2 @=? machine objectSub2

testObjectMoreMethodsTrue :: Assertion
testObjectMoreMethodsTrue = VTrue @=? machine objectMoreMethodsTrue

testObjectMoreMethods4 :: Assertion
testObjectMoreMethods4 = VInt 4 @=? machine objectMoreMethods4

testObjectInheritance13 :: Assertion
testObjectInheritance13 = VInt 13 @=? machine objectInheritance13

testObjectInheritance99 :: Assertion
testObjectInheritance99 = VInt 99 @=? machine objectInheritance99

