module Robot.Tests.Instructions where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Lang
import           Robot.Term
import           Robot.Machine.Instruction

import           Robot.Programs


transInstr :: TestTree
transInstr = testGroup "Translation to instructions"
    [ testCase "apply lambda"   testAppLambda
    , testCase "simple if"      testSimpleIf
    ]

testAppLambda :: Assertion
testAppLambda = "[IFalse,ILambda (PrgPosition 4),IApp,IReturn,ITrue,IReturn]" @=? show (term2ins . lang2term . langSimpl $ appLambda)

testSimpleIf :: Assertion
testSimpleIf = "[ITrue,IJumpIfTrue (PrgPosOffset 3),ITrue,IJump (PrgPosOffset 2),IFalse,IReturn]" @=? show (term2ins . lang2term . langSimpl $ simpleIf)
