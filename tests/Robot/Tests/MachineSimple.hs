module Robot.Tests.MachineSimple where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineSimple :: TestTree
machineSimple = testGroup "Simple programs execution"
    [ testCase     "simple if"           testSimpleIf
    , testCase     "logical expression"  testExpression
    ]

testSimpleIf    :: Assertion
testSimpleIf    = VFalse @=? machine simpleIf

testExpression   :: Assertion
testExpression   = VTrue @=? machine expressionTrue
