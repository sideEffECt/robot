module Robot.Tests.MachineCallcc where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineCallcc :: TestTree
machineCallcc = testGroup "Call-with-current-continuation"
    [ testCase     "without call/cc" testCallccNull
    , testCase     "with call/cc"    testCallcc
    ]

testCallccNull :: Assertion
testCallccNull = VInt 3 @=? machine callccNull3

testCallcc   :: Assertion
testCallcc   = VInt 2 @=? machine callcc2
