module Robot.Tests.MachineExcept where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineExcept :: TestTree
machineExcept = testGroup "Exception handling"
    [ testCase     "simple eception catch"          testExceptSimple3
    , testCase     "test witout exception thrown"   testExceptNull7
    , testCase     "conditioned throw"              testExceptSimple9
    , testCase     "throw in a sequence"            testExceptSeqnc9
    , testCase     "no throw in a sequence"         testExceptSeqnc4
    ]

testExceptSimple3 :: Assertion
testExceptSimple3 = VInt 3 @=? machine exceptSimple3

testExceptNull7 :: Assertion
testExceptNull7 = VInt 7 @=? machine exceptNull7

testExceptSimple9 :: Assertion
testExceptSimple9 = VInt 9 @=? machine exceptSimple9

testExceptSeqnc9 :: Assertion
testExceptSeqnc9 = VInt 9 @=? machine exceptSeqnc9

testExceptSeqnc4 :: Assertion
testExceptSeqnc4 = VInt 4 @=? machine exceptSeqnc4


