module Robot.Tests.Int where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Interpret.Value
import           Robot.Interpret.Eval

import           Robot.Programs


evalInt :: TestTree
evalInt = testGroup "Interpret for language with Ints"
    [ testCase "set"             testSet
    , testCase "functions"       testFuns
    , testCase "recursion"       testRec
    , testCase "factorial"       testFac
    , testCase "make-adder"      testAdder
    , testCase "make-adder rec"  testAdderRec
    , testCase "currying subber" testSubber
    ]


testSet :: Assertion
testSet = VTrue @=? eval setTrue


testFuns :: Assertion
testFuns = VInt 0 @=? eval funs0


testRec :: Assertion
testRec = VInt 0 @=? eval rec0


testFac :: Assertion
testFac = VInt 24 @=? eval fac24


testAdder :: Assertion
testAdder = VInt 12 @=? eval adder12


testAdderRec :: Assertion
testAdderRec = VInt 12 @=? eval adderRec12

testSubber :: Assertion
testSubber = VInt 2 @=? eval subber2
