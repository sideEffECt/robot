module Robot.Tests.MachineIO where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineIO :: TestTree
machineIO = testGroup "Execution with I/O"
    [ testCase     "simple read"         testSimpleRead
    , testCase     "simple write"        testSimpleWrite
    , testCase     "simple factorial"    testFactorial
    , testCase     "factorial series 0"  testFactorialSeries0
    , testCase     "factorial series 1"  testFactorialSeries1
    , testCase     "factorial series 2"  testFactorialSeries2
    ]

testSimpleRead :: Assertion
testSimpleRead = (VInt 1, []) @=? machineio [4, 3] ioReadSimple

testSimpleWrite :: Assertion
testSimpleWrite = (VUnit, [7, 13]) @=? machineio [] ioWriteSimple

testFactorial :: Assertion
testFactorial = (VUnit, [24]) @=? machineio [4] ioFac

testFactorialSeries0 :: Assertion
testFactorialSeries0 = (VUnit, []) @=? machineio [0] ioFacSeries

testFactorialSeries1 :: Assertion
testFactorialSeries1 = (VUnit, [24]) @=? machineio [1, 4] ioFacSeries

testFactorialSeries2 :: Assertion
testFactorialSeries2 = (VUnit, [1, 6, 24]) @=? machineio [3, 1, 3, 4] ioFacSeries
