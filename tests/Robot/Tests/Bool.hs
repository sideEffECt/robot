module Robot.Tests.Bool where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Interpret.Value
import           Robot.Interpret.Eval

import           Robot.Programs


evalBool :: TestTree
evalBool = testGroup "Interpret for Simple Bool language"
    [ testCase     "simple bool expression"  testExpression
    , testCase     "simple function app"     testSimpleFun
    , testCase     "multiple function app"   testMultiuseFun
    , testCase     "multiple function app 2" testMultiuseFun2
    , testCase     "multiple function app 3" testMultiuseFun3
    ]

testExpression   :: Assertion
testExpression   = VTrue @=? eval expressionTrue

testSimpleFun    :: Assertion
testSimpleFun    = VTrue @=? eval simpleFunTrue

testMultiuseFun  :: Assertion
testMultiuseFun  = VTrue @=? eval multiuseFunTrue

testMultiuseFun2 :: Assertion
testMultiuseFun2 = VTrue @=? eval multiuseFun2True

testMultiuseFun3 :: Assertion
testMultiuseFun3 = VFalse @=? eval multiuseFun3False
