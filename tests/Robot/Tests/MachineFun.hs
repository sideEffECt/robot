module Robot.Tests.MachineFun where

import           Test.Tasty
import           Test.Tasty.HUnit

import           Robot.Machine.Exec
import           Robot.Machine.Store

import           Robot.Programs


machineFun :: TestTree
machineFun = testGroup "Execution of functions"
    [ testCase     "simple bool expression"  testExpression
    , testCase     "simple function app"     testSimpleFun
    , testCase     "multiple function app"   testMultiuseFun
    , testCase     "multiple function app 2" testMultiuseFun2
    , testCase     "multiple function app 3" testMultiuseFun3
    , testCase     "set"                     testSet
    , testCase     "functions"               testFuns
    , testCase     "recursion"               testRec
    , testCase     "factorial"               testFac
    , testCase     "make-adder"              testAdder
    , testCase     "make-adder rec"          testAdderRec
    , testCase     "currying subber"         testSubber
    ]

testExpression   :: Assertion
testExpression   = VTrue @=? machine expressionTrue

testSimpleFun    :: Assertion
testSimpleFun    = VTrue @=? machine simpleFunTrue

testMultiuseFun  :: Assertion
testMultiuseFun  = VTrue @=? machine multiuseFunTrue

testMultiuseFun2 :: Assertion
testMultiuseFun2 = VTrue @=? machine multiuseFun2True

testMultiuseFun3 :: Assertion
testMultiuseFun3 = VFalse @=? machine multiuseFun3False

testSet :: Assertion
testSet = VTrue @=? machine setTrue

testFuns :: Assertion
testFuns = VInt 0 @=? machine funs0

testRec :: Assertion
testRec = VInt 0 @=? machine rec0

testFac :: Assertion
testFac = VInt 24 @=? machine fac24

testAdder :: Assertion
testAdder = VInt 12 @=? machine adder12

testAdderRec :: Assertion
testAdderRec = VInt 12 @=? machine adderRec12

testSubber :: Assertion
testSubber = VInt 2 @=? machine subber2
