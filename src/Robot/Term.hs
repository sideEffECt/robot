{-# LANGUAGE DeriveGeneric #-}

module Robot.Term ( Term(..)
                  , EnvOffset
                  , lang2term
                  , at
                  )
where

import           Data.Natural
import           GHC.Generics
import           GHC.Stack

import           Robot.Lang


newtype EnvOffset = EnvOffset Natural
    deriving (Eq, Ord, Show, Read, Generic)


data Term = TVar EnvOffset
          | TLambda Term
          | TApp Term Term
          | TUnit
          | TFalse
          | TTrue
          | TInt Int
          | TPlus Term Term
          | TMinus Term Term
          | TMul Term Term
          | TDiv Term Term
          | TEq Term Term
          | TNonEq Term Term
          | TLt Term Term
          | TLe Term Term
          | TGt Term Term
          | TGe Term Term
          | TSet EnvOffset Term
          | TIf Term Term Term
          | TSeqnc Term Term
          | TCallCC Term
          | TObject Identifier Term (Maybe Term)
          | TInvoke Term Identifier
          | TTryCatch Term Term
          | TThrow Term
          | TRead
          | TWrite Term
    deriving (Eq, Ord, Show, Read, Generic)


lang2term :: Lang -> Term
lang2term = go []
    where
        go env (LVar name)         = TVar $ position env name

        go env (LLambda [] body)           = errorWithStackTrace $ show (LLambda [] body, env)
        go env (LLambda [name] body)       = TLambda $ go (name:env) body
        go env (LLambda (name:names) body) = TLambda $ go (name:env) $ LLambda names body

        go env (LApp fun [])      = errorWithStackTrace $ show (LApp fun [], env)
        go env (LApp fun [param]) = TApp (go env fun) (go env param)
        go env (LApp fun params)  = TApp (go env (LApp fun (init params))) (go env (last params))

        go _   LUnit    = TUnit
        go _   LFalse   = TFalse
        go _   LTrue    = TTrue
        go _   (LInt i) = TInt i

        go env (LPlus ls)  = tree 0 LPlus  TPlus  env ls
        go env (LMinus ls) = tree 0 LMinus TMinus env ls
        go env (LMul ls)   = tree 1 LMul   TMul   env ls
        go env (LDiv ls)   = tree 1 LDiv   TDiv   env ls

        go env (LEq ls)    = treecmp ls LEq    TEq env
        go env (LLt ls)    = treecmp ls LLt    TLt env
        go env (LLe ls)    = treecmp ls LLe    TLe env
        go env (LGt ls)    = treecmp ls LGt    TGt env
        go env (LGe ls)    = treecmp ls LGe    TGe env
        go env (LNonEq ls) = treecmp ls LNonEq TNonEq env

        go env (LSet i l) = TSet (position env i) $ go env l

        go env (LSeqnc [])  = errorWithStackTrace $ show (LSeqnc [],  env)
        go env (LSeqnc [l]) = errorWithStackTrace $ show (LSeqnc [l],  env)
        go env (LSeqnc [l1,l2])    = TSeqnc (go env l1) $ go env l2
        go env (LSeqnc (l1:l2:ls)) = TSeqnc (go env l1) $ go env (LSeqnc $ l2:ls)

        go env (LIf cnd thn els) = TIf (go env cnd) (go env thn) (go env els)

        go env (LNot l)        = errorWithStackTrace $ show (LNot l,        env)
        go env (LLet l1 l2)    = errorWithStackTrace $ show (LLet l1 l2,    env)
        go env (LAnd ls)       = errorWithStackTrace $ show (LAnd ls,       env)
        go env (LOr ls)        = errorWithStackTrace $ show (LOr ls,        env)
        go env (LLetRec bs ls) = errorWithStackTrace $ show (LLetRec bs ls, env)

        go env (LCallCC l) = TCallCC (go env l)
        go env (LObject mt names_methods) = unrollMethods (reverse names_methods) mt env  -- TODO add "this" to env

        go env (LInvoke lobj meth []) = let tobj = go env lobj in TInvoke tobj meth
        go env (LInvoke lobj meth params) = TApp (go env (LInvoke lobj meth (init params))) (go env (last params))

        go env (LTryCatch try name catch) = TTryCatch (go env try) (go (name:env) catch)
        go env (LThrow l)                 = TThrow (go env l)

        go _   LRead      = TRead
        go env (LWrite l) = TWrite (go env l)

        tree nil _     _     _   []     = TInt nil
        tree _   lcons tcons env (l:ls) = tcons (go env l) (go env $ lcons ls)

        treecmp []  lcons _ e = errorWithStackTrace $ show (lcons [],  e)
        treecmp [l] lcons _ e = errorWithStackTrace $ show (lcons [l], e)
        treecmp [l1,l2]    _     tcons e = tcons (go e l1) (go e l2)
        treecmp (l1:l2:ls) lcons tcons e = let t1 = go e l1
                                               t2 = go e l2
                                           in TIf (tcons t1 t2) (go e (lcons (l2:ls))) TFalse

        unrollMethods [(n, m)]     mt env = TObject n (go env m) (fmap (go env) mt)
        unrollMethods ((n, m):nms) mt env = TObject n (go env m) $ Just $ unrollMethods nms mt env
        unrollMethods nms          mt env = errorWithStackTrace $ show (nms, mt, env)



position :: [Identifier] -> Identifier -> EnvOffset
position env0 name0 = go (EnvOffset 0) env0 name0
    where go (EnvOffset n) (s:env) name
              | s == name = EnvOffset n
              | otherwise = go (EnvOffset $ n+1) env name
          go _ [] _ = errorWithStackTrace $ show name0 ++ "    " ++ show env0


at :: EnvOffset -> [a] -> Maybe a
at (EnvOffset n) env
    | i >= length env = Nothing
    | otherwise       = Just $ env !! i
  where i = fromIntegral n
