module Robot.Interpret.Eval ( eval
                            , evalTerm
                            )
where

import           GHC.Stack

import qualified Data.IntMap           as M
import           Data.Maybe

import           Robot.Interpret.Value
import           Robot.Lang
import           Robot.Term


eval :: Lang -> Value
eval l = let (val, _) = evalTerm . lang2term . langSimpl $ l
         in val


evalTerm :: Term -> (Value, Store)
evalTerm t = evalwstore t [] M.empty



evalwstore :: Term -> Environment -> Store -> (Value, Store)
evalwstore TUnit    _ s = (VUnit,  s)
evalwstore TFalse   _ s = (VFalse, s)
evalwstore TTrue    _ s = (VTrue,  s)
evalwstore (TInt i) _ s = (VInt i, s)

evalwstore (TPlus  t1 t2) e s = intop (+) TPlus  t1 t2 e s
evalwstore (TMinus t1 t2) e s = intop (-) TMinus t1 t2 e s
evalwstore (TMul   t1 t2) e s = intop (*) TMul   t1 t2 e s
evalwstore (TDiv   t1 t2) e s = intop div TDiv   t1 t2 e s

evalwstore (TEq    t1 t2) e s = cmpop (==) TEq    t1 t2 e s
evalwstore (TLt    t1 t2) e s = cmpop (<)  TLt    t1 t2 e s
evalwstore (TLe    t1 t2) e s = cmpop (<=) TLe    t1 t2 e s
evalwstore (TGt    t1 t2) e s = cmpop (>)  TGt    t1 t2 e s
evalwstore (TGe    t1 t2) e s = cmpop (>=) TGe    t1 t2 e s
evalwstore (TNonEq t1 t2) e s = cmpop (/=) TNonEq t1 t2 e s

evalwstore (TIf c t1 t2) e s = case evalwstore c e s of
                                 (VTrue,  s') -> evalwstore t1 e s'
                                 (VFalse, s') -> evalwstore t2 e s'
                                 (VError err, s') -> (VError err, s')
                                 (_,          s') -> (VError $ show (TIf c t1 t2), s')

evalwstore (TVar envOffset) e s = (fromMaybe (VError $ show e ++ "\n" ++ show (TVar envOffset)) (get envOffset e s), s)

evalwstore (TSeqnc t1 t2) e s = case evalwstore t1 e s of
                                  (VError err, s') -> (VError err, s')
                                  (_,          s') -> evalwstore t2 e s'

evalwstore (TSet eo t) e s = case evalwstore t e s of
                               (VError err, s') -> (VError err, s')
                               (v,          s') -> case set v eo e s' of
                                                     (Just s'') -> (VUnit, s'')
                                                     Nothing    -> (VError $ show (TSet eo t) ++ "   " ++ show e ++ "   " ++ show s', s')

evalwstore (TLambda t) e s = (VClosure t e, s)

evalwstore (TApp f p) e s = case evalwstore p e s of
                              (VError err, s') -> (VError err, s')
                              (v,          s') -> case evalwstore f e s' of
                                                        (VClosure body env, s'') -> let (exte, exts) = push v env s''
                                                                                    in evalwstore body exte exts
                                                        (VError err,        s'') -> (VError err, s'')
                                                        (_,                 s'') -> (VError $ show (TApp f p) ++ "   " ++ show e ++ "   " ++ show s'', s'')

evalwstore TCallCC{}   _ _ = errorWithStackTrace "eval doesn't support CallCC"
evalwstore TObject{}   _ _ = errorWithStackTrace "eval doesn't support Object"
evalwstore TInvoke{}   _ _ = errorWithStackTrace "eval doesn't support Invoke"
evalwstore TThrow{}    _ _ = errorWithStackTrace "eval doesn't support Throw"
evalwstore TTryCatch{} _ _ = errorWithStackTrace "eval doesn't support TryCatch"
evalwstore TRead{}     _ _ = errorWithStackTrace "eval doesn't support Read"
evalwstore TWrite{}    _ _ = errorWithStackTrace "eval doesn't support Write"


intop :: (Int -> Int -> Int) -> (Term -> Term -> Term) -> Term -> Term -> Environment -> Store -> (Value, Store)
intop op = numop (\x y -> VInt $ x `op` y)


cmpop :: (Int -> Int -> Bool) -> (Term -> Term -> Term) -> Term -> Term -> Environment -> Store -> (Value, Store)
cmpop op = numop (\x y -> if x `op` y then VTrue else VFalse)


numop :: (Int -> Int -> Value) -> (Term -> Term -> Term) -> Term -> Term -> Environment -> Store -> (Value, Store)
numop op opcons t1 t2 e s = case evalwstore t1 e s of
                              (VInt i1, s') -> case evalwstore t2 e s' of
                                                     (VInt i2, s'') -> (i1 `op` i2, s'')
                                                     (VError err, s'') -> (VError err, s'')
                                                     (_,          s'') -> (VError $ show (opcons t1 t2), s'')
                              (VError err, s') -> (VError err,                   s')
                              (_,          s') -> (VError $ show (opcons t1 t2), s')
