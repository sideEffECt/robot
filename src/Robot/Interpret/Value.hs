module Robot.Interpret.Value ( Value(..)
                             , Store
                             , Environment
                             , get
                             , push
                             , set
                             )
where

import qualified Data.IntMap  as M
import qualified Data.IntSet  as S
import           Data.Natural

import           Robot.Term


data Value = VClosure Term Environment
           | VUnit
           | VFalse
           | VTrue
           | VInt Int
           | VError String
    deriving (Eq, Show, Read)


newtype StoLocation = StoLocation Natural
    deriving (Eq, Show, Read, Ord)


type Store = M.IntMap Value

type Environment = [StoLocation]


get :: EnvOffset -> Environment -> Store -> Maybe Value
get eo e s = do a <- at eo e
                value a s
  where value (StoLocation i) = M.lookup (fromIntegral i)


set :: Value -> EnvOffset -> Environment -> Store -> Maybe Store
set v eo e s = do a <- at eo e
                  return $ put a s
  where put (StoLocation i) = M.insert (fromIntegral i) v


push :: Value -> Environment -> Store -> (Environment, Store)
push v e s = let a           = newStoLocation
                 (StoLocation i) = a
             in  (a:e, M.insert (fromIntegral i) v s)
  where newStoLocation
            | M.null s  = StoLocation 0
            | otherwise = StoLocation . fromIntegral . (+1) . S.findMax . M.keysSet $ s
