module Robot.StaticEvaluator where

import Data.Maybe

import Robot.Term


staticeval :: Term -> Term
staticeval = go []
  where go env x@(TVar o) = case at o env of
                              Just TUnit      -> TUnit
                              Just TFalse     -> TFalse
                              Just TTrue      -> TTrue
                              Just i@(TInt _) -> i
                              _               -> x
        go env (TLambda t) = TLambda $ go env t
        go env (TApp f a) = let ae = go env a
                                fe = go (ae:env) f
                            in  case fe of
                                  TLambda TUnit      -> TUnit
                                  TLambda TFalse     -> TFalse
                                  TLambda TTrue      -> TTrue
                                  TLambda x@(TInt _) -> x
                                  TLambda x          -> if hasBoundVars [] x
                                                           then x
                                                           else TApp fe ae
                                  _                  -> TApp f a

        go _   TUnit = TUnit
        go _   TFalse = TFalse
        go _   TTrue = TTrue
        go _   x@(TInt _) = x

        go env (TPlus  t1 t2) = evalnum (+) TPlus  env t1 t2
        go env (TMinus t1 t2) = evalnum (-) TMinus env t1 t2
        go env (TMul   t1 t2) = evalnum (*) TMul   env t1 t2
        go env (TDiv   t1 t2) = evalnum div TDiv   env t1 t2

        go env (TEq    t1 t2) = evalcmp (==) TEq    env t1 t2
        go env (TNonEq t1 t2) = evalcmp (/=) TNonEq env t1 t2
        go env (TLt    t1 t2) = evalcmp (<)  TLt    env t1 t2
        go env (TLe    t1 t2) = evalcmp (<=) TLe    env t1 t2
        go env (TGt    t1 t2) = evalcmp (>)  TGt    env t1 t2
        go env (TGe    t1 t2) = evalcmp (>=) TGe    env t1 t2

        go env (TIf c t e) = let ce = go env c
                                 te = go env t
                                 ee = go env e
                             in  case ce of
                                   TTrue  -> te
                                   TFalse -> ee
                                   _      -> TIf ce te ee
        go env (TSeqnc t1 t2) = TSeqnc (go env t1) (go env t2)

        go _   TRead = TRead
        go env (TWrite t) = TWrite (go env t)
        go env (TSet o t) = TSet o (go env t)

        go _   x@(TCallCC _) = x
        go _   x@(TObject{}) = x
        go _   x@(TInvoke _ _) = x
        go _   x@(TTryCatch _ _) = x
        go _   x@(TThrow _) = x


        evalnum :: (Int -> Int -> Int) -> (Term -> Term -> Term) -> [Term] -> Term -> Term -> Term
        evalnum f constructor env t1 t2 = let te1 = go env t1
                                              te2 = go env t2
                                          in  case (te1, te2) of
                                                (TInt i1, TInt i2) -> TInt (i1 `f` i2)
                                                _                  -> constructor te1 te2

        evalcmp :: (Int -> Int -> Bool) -> (Term -> Term -> Term) -> [Term] -> Term -> Term -> Term
        evalcmp f constructor env t1 t2 = let te1 = go env t1
                                              te2 = go env t2
                                          in  case (te1, te2) of
                                                (TInt i1, TInt i2) -> if i1 `f` i2 then TTrue else TFalse
                                                _                  -> constructor te1 te2


hasBoundVars :: [Term] -> Term -> Bool
hasBoundVars env (TVar o) =  isJust (at o env)
hasBoundVars env (TLambda t) = hasBoundVars env t
hasBoundVars env (TApp f a) = hasBoundVars (a:env) f && hasBoundVars env a

hasBoundVars _   TUnit    = True
hasBoundVars _   TFalse   = True
hasBoundVars _   TTrue    = True
hasBoundVars _   (TInt _) = True

hasBoundVars env (TPlus  t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TMinus t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TMul   t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TDiv   t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TEq    t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TNonEq t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TLt    t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TLe    t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TGt    t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars env (TGe    t1 t2) = hasBoundVars env t1 && hasBoundVars env t2

hasBoundVars env (TSet o t) = isJust (at o env) && hasBoundVars env t
hasBoundVars env (TIf c t e) = hasBoundVars env c && hasBoundVars env t && hasBoundVars env e
hasBoundVars env (TSeqnc t1 t2) = hasBoundVars env t1 && hasBoundVars env t2
hasBoundVars _   TRead = True
hasBoundVars env (TWrite t) = hasBoundVars env t

hasBoundVars _   (TCallCC _) = False
hasBoundVars _   (TObject{}) = False
hasBoundVars _   (TInvoke _ _) = False
hasBoundVars _   (TTryCatch _ _) = False
hasBoundVars _   (TThrow _) = False
