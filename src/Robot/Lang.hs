{-# LANGUAGE DeriveGeneric #-}

module Robot.Lang ( Lang(..)
                  , Identifier
                  , langSimpl
                  )
where

import           GHC.Generics
import           GHC.Stack


import Data.List
import qualified Data.Map.Strict as M
import Data.Maybe


type Identifier = String


data Lang = LVar Identifier
          | LLambda [Identifier] Lang
          | LApp Lang [Lang]
          | LUnit
          | LFalse
          | LTrue
          | LInt Int
          | LIf Lang Lang Lang
          | LPlus [Lang]
          | LMinus [Lang]
          | LMul [Lang]
          | LDiv [Lang]
          | LEq [Lang]
          | LLt [Lang]
          | LLe [Lang]
          | LGt [Lang]
          | LGe [Lang]
          | LNonEq [Lang]
          | LSet Identifier Lang
          | LSeqnc [Lang]
          | LCallCC Lang
          | LObject (Maybe Lang) [(Identifier, Lang)]
          | LInvoke Lang Identifier [Lang]
          | LTryCatch Lang Identifier Lang
          | LThrow Lang
          | LRead
          | LWrite Lang

          | LLet [(Identifier, Lang)] Lang
          | LLetRec [(Identifier, Lang)] Lang
          | LNot Lang
          | LAnd [Lang]
          | LOr [Lang]
    deriving (Eq, Ord, Show, Read, Generic)


langSimpl :: Lang -> Lang
langSimpl (LLet    names_assigns body) = nestLets     (optimizeLets names_assigns) body
langSimpl (LLetRec names_assigns body) = expandLetrec names_assigns body

langSimpl (LNot lang)               = LIf (langSimpl lang) LFalse         LTrue
langSimpl (LAnd [])                 = LTrue
langSimpl (LAnd (l:ls))             = LIf (langSimpl l)   (langSimpl $ LAnd ls) LFalse
langSimpl (LOr [])                  = LFalse
langSimpl (LOr (l:ls))              = LIf (langSimpl l)   LTrue                 (langSimpl $ LOr ls)

langSimpl (LLambda ids l)           = LLambda ids $ langSimpl l
langSimpl (LApp l ls)               = LApp (langSimpl l) (map langSimpl ls)
langSimpl (LIf c t e)               = LIf (langSimpl c) (langSimpl t) (langSimpl e)
langSimpl (LPlus ls)                = LPlus  $ map langSimpl ls
langSimpl (LMinus ls)               = LMinus $ map langSimpl ls
langSimpl (LMul ls)                 = LMul   $ map langSimpl ls
langSimpl (LDiv ls)                 = LDiv   $ map langSimpl ls
langSimpl (LEq ls)                  = LEq    $ map langSimpl ls
langSimpl (LLt ls)                  = LLt    $ map langSimpl ls
langSimpl (LLe ls)                  = LLe    $ map langSimpl ls
langSimpl (LGt ls)                  = LGt    $ map langSimpl ls
langSimpl (LGe ls)                  = LGe    $ map langSimpl ls
langSimpl (LNonEq ls)               = LNonEq $ map langSimpl ls
langSimpl (LSet i l)                = LSet i $ langSimpl l
langSimpl (LSeqnc ls)               = LSeqnc $ map langSimpl ls
langSimpl (LCallCC l)               = LCallCC $ langSimpl l
langSimpl (LObject l names_assigns) = LObject (fmap langSimpl l) $ expandMethods names_assigns
langSimpl (LInvoke ml ident ls)     = LInvoke (langSimpl ml) ident (map langSimpl ls)
langSimpl (LTryCatch t n c)         = LTryCatch (langSimpl t) n (langSimpl c)
langSimpl (LThrow t)                = LThrow (langSimpl t)
langSimpl LRead                     = LRead
langSimpl (LWrite t)                = LWrite (langSimpl t)

langSimpl (LVar i)                  = LVar i
langSimpl LUnit                     = LUnit
langSimpl LFalse                    = LFalse
langSimpl LTrue                     = LTrue
langSimpl (LInt i)                  = LInt i


nestLets :: [(Identifier, Lang)] -> Lang -> Lang
nestLets names_assigns body = go (map fst names_assigns) (map (langSimpl . snd) names_assigns)
  where go []     []       = body
        go (n:ns) (a:asgs) = LApp (LLambda [n] (go ns asgs)) [a]
        go _      _        = errorWithStackTrace $ show names_assigns ++ "   " ++ show body


expandLetrec :: [(Identifier, Lang)] -> Lang -> Lang
expandLetrec names_assigns body = let names   = map fst               names_assigns
                                      assigns = map (langSimpl . snd) names_assigns
                                      sets    = zipWith LSet names assigns
                                      units   = map (const LUnit) assigns
                                  in  LApp (LLambda names (LSeqnc $ sets ++ [body])) units


expandMethods :: [(Identifier, Lang)] -> [(Identifier, Lang)]
expandMethods names_assigns = zip (map fst names_assigns) (map (langSimpl . LLambda ["this"] . snd) names_assigns)  -- TODO remove "this





---------------------------------------



isSubsequenceOf :: (Eq a) => [a] -> [a] -> Bool
isSubsequenceOf []    _                    = True
isSubsequenceOf _     []                   = False
isSubsequenceOf a@(x:a') (y:b) | x == y    = isSubsequenceOf a' b
                               | otherwise = isSubsequenceOf a b


flatten :: Lang -> Lang
flatten (LPlus ls)  = LPlus . sort $ members summands (map flatten ls)
flatten (LMul  ls)  = LMul  . sort $ members factors  (map flatten ls)
flatten (LMinus ls) = LMinus $ map flatten ls
flatten (LDiv ls)   = LDiv $ map flatten ls
flatten x = x


summands :: Lang -> Maybe [Lang]
summands (LPlus xs) = Just xs
summands _          = Nothing


factors :: Lang -> Maybe [Lang]
factors (LMul xs) = Just xs
factors _         = Nothing


members :: (Lang -> Maybe [Lang]) -> [Lang] -> [Lang]
members destrMember (x:xs) = case destrMember x of
                               Just ys -> ys ++ members destrMember xs
                               Nothing -> x : members destrMember xs
members _ [] = []









expressions :: Lang -> [Lang]
expressions e@(LPlus  _) = nub . expressions' $ e
expressions e@(LMul   _) = nub . expressions' $ e
expressions e@(LMinus _) = nub . expressions' $ e
expressions e@(LDiv   _) = nub . expressions' $ e
expressions e = [e]


expressions' :: Lang -> [Lang]
expressions' e@(LPlus  ls) = concatMap expressions' ls ++ [e]
expressions' e@(LMul   ls) = concatMap expressions' ls ++ [e]
expressions' e@(LMinus ls) = concatMap expressions' ls ++ [e]
expressions' e@(LDiv   ls) = concatMap expressions' ls ++ [e]
expressions' _ = []



unrollExpressions :: M.Map Lang String -> [Lang] -> ([(String, Lang)], M.Map Lang String)
unrollExpressions m (l:ls) = let newTmpVar = "t" ++ show (M.size m)
                                 (xs,m') = unrollExpressions (M.insert l newTmpVar m) ls
                             in  ((newTmpVar, substitute m l) : xs, m')
unrollExpressions m [] = ([], m)


substitute :: M.Map Lang String -> Lang -> Lang
substitute m x = if M.member x m
                    then LVar . fromJust $ M.lookup x m
                    else case x of
                           (LPlus ls) -> LPlus $ case reverse $ filter (\y -> case y of
                                                                                (LPlus xs) -> xs `isSubsequenceOf` ls
                                                                                _          -> False) (M.keys m) of
                                                   []             -> map (substitute m) ls
                                                   (LPlus ys : _) -> (LVar . fromJust $ M.lookup (LPlus ys) m) : (case substitute m (LPlus $ listMinus ls ys) of
                                                                                                                    LPlus subtitutedList -> subtitutedList
                                                                                                                    err                  -> errorWithStackTrace $ show err)
                                                   _              -> ls
                           (LMul ls) -> LMul $ case reverse $ filter (\y -> case y of
                                                                                (LMul xs) -> xs `isSubsequenceOf` ls
                                                                                _         -> False) (M.keys m) of
                                                 []            -> map (substitute m) ls
                                                 (LMul ys : _) -> (LVar . fromJust $ M.lookup (LMul ys) m) : (case substitute m (LMul $ listMinus ls ys) of
                                                                                                                LMul subtitutedList -> subtitutedList
                                                                                                                err                 -> errorWithStackTrace $ show err)
                                                 _             -> ls
                           (LMinus ls) -> LMinus $ map (substitute m) ls
                           (LDiv ls  ) -> LDiv   $ map (substitute m) ls
                           y -> y


-- xs - ys
listMinus :: Eq a => [a] -> [a] -> [a]
listMinus = foldl (flip delete)




optimizeLet :: M.Map Lang String -> (String, Lang) -> ([(String, Lang)], M.Map Lang String)
optimizeLet m (var, body) = let (unrolls,m') = unrollExpressions m (filterComputed . expressions . flatten $ body)
                                (_, x) = last unrolls
                            in  (init unrolls ++ [(var, x)],M.insert body var m')
  where filterComputed = filter (\x -> x `notElem` M.keys m)


optimizeLets :: [(String, Lang)] -> [(String, Lang)]
optimizeLets = go M.empty
  where go :: M.Map Lang String -> [(String, Lang)] -> [(String, Lang)]
        go m (let_:lets) = let (lets', m') = optimizeLet m let_
                           in  lets' ++ go m' lets
        go _ [] = []



