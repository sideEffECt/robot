module Robot.Machine.Instruction ( Instruction (..)
                                 , PrgPosition
                                 , PrgPosOffset
                                 , getInstr
                                 , translate
                                 , term2ins
                                 , next
                                 , jump
                                 , initPos
                                 )
where

import           GHC.Stack

import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.Natural
import qualified Data.Vector     as V

import           Robot.Term


newtype PrgPosition = PrgPosition Natural
    deriving (Eq, Show, Read, Ord)

newtype PrgPosOffset = PrgPosOffset Natural
    deriving (Eq, Show, Read, Ord)


data Instruction = IVar EnvOffset
                 | ILambda PrgPosition
                 | IApp
                 | IReturn
                 | IUnit
                 | IFalse
                 | ITrue
                 | IInt Int
                 | IPlus
                 | IMinus
                 | IMul
                 | IDiv
                 | IEq
                 | INonEq
                 | ILt
                 | ILe
                 | IGt
                 | IGe
                 | ISet EnvOffset
                 | IJump PrgPosOffset
                 | IJumpIfTrue PrgPosOffset
                 | IPop
                 | ICallCC
                 | IObject String PrgPosition (Maybe PrgPosition)
                 | IInvoke String
                 | ITryCatch PrgPosition
                 | IThrow
                 | IRead
                 | IWrite
    deriving (Eq, Show, Read, Ord)


translate :: Term -> V.Vector Instruction
translate = V.fromList . term2ins


term2ins :: Term -> [Instruction]
term2ins t0 = let ts = lambdas t0
                  l = layout ts
                  is = map (lambda2ins $ getPosition l) ts
              in  concat is
  where getPosition m t = fromMaybe (errorWithStackTrace $ "cannot find position of term" ++ "   " ++ show t ++ "  " ++ show m) (M.lookup t m)



jumpBehind :: [Instruction] -> PrgPosOffset
jumpBehind = PrgPosOffset . fromIntegral . (+1) . length



lambdas :: Term -> [Term]
lambdas t0 = t0 : go t0
  where go (TLambda t)    = t : go t
        go (TSet _ t)     = go t
        go (TCallCC t)    = go t
        go (TSeqnc t1 t2) = go t1 ++ go t2
        go (TApp t1 t2)   = go t1 ++ go t2
        go (TIf tc tt te) = go tc ++ go tt ++ go te
        go (TObject _ t p) = maybeToList p ++ maybe [] go p ++ {-[t] ++-} go t    -- TODO
        go (TInvoke t _) = go t
        go (TTryCatch t c) = go t ++ [c] ++ go c
        go (TThrow t)      = go t
        go TRead      = []
        go (TWrite t) = go t
        go (TVar _)    = []
        go TUnit       = []
        go TFalse      = []
        go TTrue       = []
        go (TInt _)    = []
        go (TPlus t1 t2)  = go t1 ++ go t2
        go (TMinus t1 t2) = go t1 ++ go t2
        go (TMul t1 t2)   = go t1 ++ go t2
        go (TDiv t1 t2)   = go t1 ++ go t2
        go (TEq t1 t2)    = go t1 ++ go t2
        go (TNonEq t1 t2) = go t1 ++ go t2
        go (TLt t1 t2)    = go t1 ++ go t2
        go (TLe t1 t2)    = go t1 ++ go t2
        go (TGt t1 t2)    = go t1 ++ go t2
        go (TGe t1 t2)    = go t1 ++ go t2


layout :: [Term] -> M.Map Term PrgPosition
layout ts = let instructions = map (lambda2ins (const $ PrgPosition 0)) ts
                sizes        = map length instructions
                offsets      = scanl (+) 0 sizes
                positions    = map (PrgPosition . fromIntegral) offsets
                pairs        = zip ts positions
            in  M.fromList pairs


lambda2ins :: (Term -> PrgPosition) -> Term -> [Instruction]
lambda2ins pos t0 = go t0 ++ [IReturn]
  where go (TLambda t)    = [ILambda $ pos t]
        go (TSet o t)     = go t ++ [ISet o]
        go (TCallCC t)    = go t ++ [ICallCC]
        go (TSeqnc t1 t2) = go t1 ++ [IPop] ++ go t2
        go (TApp t1 t2)   = go t2 ++ go t1 ++ [IApp]
        go (TIf tc tt te) = let it = go tt
                                ie = go te ++ [IJump $ jumpBehind it]
                                ic = go tc ++ [IJumpIfTrue $ jumpBehind ie]
                            in  ic ++ ie ++ it
        go (TObject methodname (TLambda method) parent) = [IObject methodname (pos method) (fmap pos parent)]
        go (TObject methodname invalidmethod    parent) = errorWithStackTrace $ show (TObject methodname invalidmethod    parent)  -- TODO remove "this"
        go (TInvoke object methodname) = go object ++ [IInvoke methodname]
        go (TTryCatch try catch) = ITryCatch (pos catch) : go try
        go (TThrow t)            = go t ++ [IThrow]
        go TRead      = [IRead]
        go (TWrite t) = go t ++ [IWrite]
        go (TVar o)    = [IVar o]
        go TUnit       = [IUnit]
        go TFalse      = [IFalse]
        go TTrue       = [ITrue]
        go (TInt n)    = [IInt n]
        go (TPlus t1 t2)  = go t1 ++ go t2 ++ [IPlus]
        go (TMinus t1 t2) = go t1 ++ go t2 ++ [IMinus]
        go (TMul t1 t2)   = go t1 ++ go t2 ++ [IMul]
        go (TDiv t1 t2)   = go t1 ++ go t2 ++ [IDiv]
        go (TEq t1 t2)    = go t1 ++ go t2 ++ [IEq]
        go (TNonEq t1 t2) = go t1 ++ go t2 ++ [INonEq]
        go (TLt t1 t2)    = go t1 ++ go t2 ++ [ILt]
        go (TLe t1 t2)    = go t1 ++ go t2 ++ [ILe]
        go (TGt t1 t2)    = go t1 ++ go t2 ++ [IGt]
        go (TGe t1 t2)    = go t1 ++ go t2 ++ [IGe]


getInstr :: V.Vector Instruction -> PrgPosition -> Maybe Instruction
getInstr is (PrgPosition n)
    | i >= V.length is = Nothing
    | otherwise      = Just $ is V.! i
  where i = fromIntegral n


next :: PrgPosition -> PrgPosition
next (PrgPosition n) = PrgPosition $ n + 1


jump :: PrgPosOffset -> PrgPosition -> PrgPosition
jump (PrgPosOffset offset) (PrgPosition pos) = PrgPosition $ pos + offset


initPos :: PrgPosition
initPos = PrgPosition 0


