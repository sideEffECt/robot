module Robot.Machine.Semantics ( pushVal
                               , pushVar
                               , intOp
                               , intCmpOp
                               , set
                               , jumpIf
                               , pop
                               , lambda
                               , object
                               , trycatch
                               , app
                               , callcc
                               , invoke
                               , throw
                               , write
                               , ret
                               )
where

import           Robot.Machine.Instruction
import           Robot.Machine.Store
import           Robot.Term


pushVal :: Value -> StckLocation -> Store -> (StckLocation, Store)
pushVal = pushToStack  -- FIXME maybe more abstract


pushVar :: EnvOffset -> EnvLocation -> StckLocation -> Store -> Maybe (StckLocation, Store)
pushVar eo eloc sloc store = do vlocs <- envList eloc store
                                vloc <- at eo vlocs
                                v <- getVal store vloc
                                return $ pushToStack v sloc store


numOp :: (Value -> Value -> Maybe Value) -> StckLocation -> Store -> Maybe (StckLocation, Store)
numOp liftf sloc store = do (v2, sloc')  <- popFromStack sloc  store
                            (v1, sloc'') <- popFromStack sloc' store
                            res <- v1 `liftf` v2
                            return $ pushToStack res sloc'' store


intOp :: (Int -> Int -> Int) -> StckLocation -> Store -> Maybe (StckLocation, Store)
intOp f = numOp intLiftF
  where intLiftF (VInt n1) (VInt n2) = Just . VInt $ n1 `f` n2
        intLiftF _         _         = Nothing


intCmpOp :: (Int -> Int -> Bool) -> StckLocation -> Store -> Maybe (StckLocation, Store)
intCmpOp f = numOp boolLiftF
    where boolLiftF (VInt n1) (VInt n2) = Just $ if n1 `f` n2 then VTrue else VFalse
          boolLiftF _         _         = Nothing


set :: EnvOffset -> EnvLocation -> StckLocation -> Store -> Maybe (StckLocation, Store)
set eo eloc sloc store = do vlocs      <- envList eloc store
                            vloc       <- at eo vlocs
                            (v, sloc') <- popFromStack sloc store
                            let store' = putVal vloc v store
                            let (sloc'', store'') = pushToStack VUnit sloc' store'
                            return (sloc'', store'')



jumpIf :: PrgPosOffset -> PrgPosition -> StckLocation -> Store -> Maybe (PrgPosition, StckLocation)
jumpIf ppo pos sloc store = do (v, sloc') <- popFromStack sloc store
                               cond <- destrBool v
                               return (if cond then jump ppo pos else next pos, sloc')
  where destrBool VFalse = Just False
        destrBool VTrue  = Just True
        destrBool _      = Nothing


pop :: StckLocation -> Store -> Maybe StckLocation
pop sloc store = do (_, sloc') <- popFromStack sloc store
                    return sloc'


lambda :: PrgPosition -> EnvLocation -> StckLocation -> Store -> (StckLocation, Store)
lambda fun eloc sloc store = let closure = VClosure fun eloc
                             in  pushToStack closure sloc store


object :: String -> PrgPosition -> Maybe PrgPosition -> EnvLocation -> StckLocation -> Store -> (StckLocation, Store)
object mname method parent eloc sloc store = let obj = VObject mname method parent eloc
                                             in  pushToStack obj sloc store


trycatch :: PrgPosition -> EnvLocation -> ContLocation -> Store -> (ContLocation, Store)
trycatch = pushToContTrycatch


callcc :: (PrgPosition, EnvLocation, StckLocation, Store, ContLocation) -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
callcc = apply makeCont
  where makeCont pos eloc sloc _ cloc = Just (VCont (App (next pos) eloc sloc cloc), sloc)


app :: (PrgPosition, EnvLocation, StckLocation, Store, ContLocation) -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
app  = apply popParam
  where popParam _ _ sloc store _ = popFromStack sloc store


apply :: (PrgPosition -> EnvLocation -> StckLocation -> Store -> ContLocation -> Maybe (Value, StckLocation)) -> (PrgPosition, EnvLocation, StckLocation, Store, ContLocation) -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
apply getParam (pos, eloc, sloc, store, cloc) = do (f,   sloc')  <- popFromStack sloc store
                                                   (par, sloc'') <- getParam pos eloc sloc' store cloc
                                                   case f of
                                                     (VClosure fun env)             -> Just $ applyClosure par fun env (pos, eloc, sloc'', cloc) store
                                                     (VCont (App r env stack cont)) -> Just $ ret par (r, env, stack, cont) store
                                                     _                              -> Nothing


applyClosure :: Value -> PrgPosition -> EnvLocation -> (PrgPosition, EnvLocation, StckLocation, ContLocation) -> Store -> (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
applyClosure par fpos feloc (pos, eloc, sloc, cloc) store = let (feloc', store') = pushToEnvironment par feloc store
                                                                (cloc', store'') = pushToContApp (next pos) eloc sloc cloc store'
                                                            in  (fpos, feloc', initStack, store'', cloc')


invoke :: String -> (PrgPosition, EnvLocation, StckLocation, Store, ContLocation) -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
invoke methodName (pos, eloc, sloc, store, cloc) = do (obj, sloc') <- popFromStack sloc store
                                                      (metname, metpos, parent, metenv) <- destrObject obj
                                                      if methodName == metname
                                                        then case peekContInvoke pos cloc store of
                                                               Just (metname', metpos', parent', metenv', cloc') -> Just $ applyClosure (VObject metname' metpos' parent' metenv') metpos metenv (pos, eloc, sloc', cloc') store
                                                               Nothing -> Just $ applyClosure obj metpos metenv (pos, eloc, sloc', cloc) store
                                                        else do p <- parent
                                                                let (cloc', store') = case peekContInvoke pos cloc store of
                                                                                        Just _  -> (cloc, store)
                                                                                        Nothing -> pushToContInvoke pos metname metpos parent metenv cloc store
                                                                let (cloc'', store'') = pushToContApp pos eloc sloc' cloc' store'
                                                                return (p, metenv, initStack, store'', cloc'')
  where destrObject (VObject n m p e) = Just (n, m, p, e)
        destrObject _                 = Nothing


throw :: StckLocation -> ContLocation -> Store -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
throw sloc cloc store = do (ex, _) <- popFromStack sloc store
                           (catchpos, exeloc, cloc') <- getFirstTryCatch cloc store
                           let (exeloc', store') = pushToEnvironment ex exeloc store
                           return (catchpos, exeloc', initStack, store', cloc')


write :: StckLocation -> Store -> Maybe (StckLocation, Store, Value)
write sloc store = do (v, sloc') <- popFromStack sloc store
                      let (sloc'', store') = pushToStack VUnit sloc' store
                       in  return (sloc'', store', v)


ret :: Value -> (PrgPosition, EnvLocation, StckLocation, ContLocation) -> Store -> (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)
ret v (pos, eloc, sloc, cloc) store = let (sloc', store') = pushToStack v sloc store
                                      in  (pos, eloc, sloc', store', cloc)



