module Robot.Machine.Exec ( run
                          , runio
                          , machine
                          , machineio
                          )
where

import           Data.Vector

import           Robot.Lang
import           Robot.Term
import           Robot.Machine.Instruction
import           Robot.Machine.Step
import           Robot.Machine.Store


machine :: Lang -> Value
machine l = run . translate . lang2term . langSimpl $ l


machineio :: [Int] -> Lang -> (Value, [Int])
machineio input l = runio input . translate . lang2term . langSimpl $ l


run :: Vector Instruction -> Value
run is = fst $ exec is (initState [])


runio :: [Int] -> Vector Instruction -> (Value, [Int])
runio input is = exec is (initState input)


exec :: Vector Instruction -> State -> (Value, [Int])
exec is = go
  where go s = case step is s of
                 Left s'           -> go s'
                 Right (v, output) -> (v, output)


initState :: [Int] -> State
initState input = (initPos, initEnv, initStack, initStore, initCont, input, [])







