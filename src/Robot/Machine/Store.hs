module Robot.Machine.Store ( Store
                           , Value (..)
                           , Continuation (..)
                           , EnvLocation
                           , StckLocation
                           , ContLocation
                           , pushToStack
                           , popFromStack
                           , pushToEnvironment
                           , pushToContApp
                           , pushToContTrycatch
                           , pushToContInvoke
                           , peekContInvoke
                           , getVal
                           , putVal
                           , getCont
                           , envList
                           , getFirstApp
                           , getFirstTryCatch
                           , singletonStack
                           , initEnv
                           , initStack
                           , initStore
                           , initCont
                           )
where


import qualified Data.IntMap               as M
import           Data.Natural

import           Robot.Machine.Instruction


type StoLocation = Natural


data Store = Store (M.IntMap Box) StoLocation
    deriving (Eq, Show, Read)


newtype ValLocation = ValLocation StoLocation
    deriving (Eq, Show, Read, Ord)

newtype EnvLocation = EnvLocation StoLocation
    deriving (Eq, Show, Read, Ord)

newtype StckLocation = StckLocation StoLocation
    deriving (Eq, Show, Read, Ord)

newtype ContLocation = ContLocation StoLocation
    deriving (Eq, Show, Read, Ord)


data Value = VClosure PrgPosition EnvLocation
           | VUnit
           | VFalse
           | VTrue
           | VInt Int
           | VCont Continuation
           | VObject String PrgPosition (Maybe PrgPosition) EnvLocation
    deriving (Eq, Show, Read)


data Continuation = App PrgPosition EnvLocation StckLocation ContLocation
                  | Invoke PrgPosition String PrgPosition (Maybe PrgPosition) EnvLocation ContLocation
                  | TryCatch PrgPosition EnvLocation ContLocation
    deriving (Eq, Show, Read)


data Box = Val Value
         | Env  ValLocation EnvLocation
         | Stck Value StckLocation
         | Null
    deriving (Eq, Show, Read)


new :: Box -> Store -> (StoLocation, Store)
new b (Store s top) = (top, Store (M.insert (fromIntegral top) b s) (top+1))


get :: StoLocation -> Store -> Maybe Box
get loc (Store s _) = M.lookup (fromIntegral loc) s


put :: StoLocation -> Box -> Store -> Store
put loc b (Store store top) = Store (M.insert (fromIntegral loc) b store) top


putVal :: ValLocation -> Value -> Store -> Store
putVal (ValLocation vloc) v = put vloc (Val v)


pushToStack :: Value -> StckLocation -> Store -> (StckLocation, Store)
pushToStack v sloc store = let (sloc', store') = new (Stck v sloc) store
                           in  (StckLocation sloc', store')


popFromStack :: StckLocation -> Store -> Maybe (Value, StckLocation)
popFromStack (StckLocation sloc) store = do box <- get sloc store
                                            destrStck box


pushToEnvironment :: Value -> EnvLocation -> Store -> (EnvLocation, Store)
pushToEnvironment v eloc store = let (vloc,  store')  = new (Val v)                       store
                                     (eloc', store'') = new (Env (ValLocation vloc) eloc) store'
                                 in  (EnvLocation eloc', store'')


pushToCont :: Continuation -> Store -> (ContLocation, Store)
pushToCont cont store = let (cloc', store') = new (Val . VCont $ cont) store
                        in  (ContLocation cloc', store')


pushToContApp :: PrgPosition -> EnvLocation -> StckLocation -> ContLocation -> Store -> (ContLocation, Store)
pushToContApp pos eloc sloc cloc = pushToCont $ App pos eloc sloc cloc


pushToContTrycatch :: PrgPosition -> EnvLocation -> ContLocation -> Store -> (ContLocation, Store)
pushToContTrycatch catchpos eloc cloc = pushToCont $ TryCatch catchpos eloc cloc


pushToContInvoke :: PrgPosition -> String -> PrgPosition -> Maybe PrgPosition -> EnvLocation -> ContLocation -> Store -> (ContLocation, Store)
pushToContInvoke callsite mname meth parent menv cloc = pushToCont $ Invoke callsite mname meth parent menv cloc


peekContInvoke :: PrgPosition -> ContLocation -> Store -> Maybe (String, PrgPosition, Maybe PrgPosition, EnvLocation, ContLocation)
peekContInvoke callsite (ContLocation loc) store = case get loc store of  -- maybe go arbitrarily deeper when searching for ???
                                                     Just (Val (VCont (Invoke cs n m p e c))) -> if cs == callsite then Just (n, m, p, e, c) else Nothing
                                                     Just (Val (VCont App{}))            -> Nothing
                                                     Just (Val (VCont (TryCatch _ _ c)))    -> peekContInvoke callsite c store
                                                     _                                   -> Nothing


getVal :: Store -> ValLocation -> Maybe Value
getVal store (ValLocation loc) = case get loc store of
                                   Just (Val v) -> Just v
                                   _            -> Nothing


getCont :: Store -> ContLocation -> Maybe Continuation
getCont store (ContLocation loc) = case get loc store of
                                     Just (Val (VCont c)) -> Just c
                                     _                    -> Nothing


envList :: EnvLocation -> Store -> Maybe [ValLocation]
envList (EnvLocation stck0) store = sequence $ chain stck0
  where chain stck = case get stck store of
                       Just Null                        -> []
                       Just (Env v (EnvLocation stck')) -> Just v : chain stck'
                       _                                -> [Nothing]


singletonStack :: StckLocation -> Store -> Maybe Value
singletonStack (StckLocation sloc) store = do b <- get sloc store
                                              (v, StckLocation sloc') <- destrStck b
                                              if isNull sloc'
                                                then Just v
                                                else Nothing
  where isNull cloc = case get cloc store of
                        Just Null -> True
                        _         -> False


getFirstApp :: ContLocation -> Store -> Maybe (PrgPosition, EnvLocation, StckLocation, ContLocation)
getFirstApp cloc store = do cont <- getCont store cloc
                            case cont of
                              (TryCatch _ _ cloc')        -> getFirstApp cloc' store
                              (App pos eloc sloc cloc') -> Just (pos, eloc, sloc, cloc')
                              Invoke{}                  -> Nothing


getFirstTryCatch :: ContLocation -> Store -> Maybe (PrgPosition, EnvLocation, ContLocation)
getFirstTryCatch cloc store = do cont <- getCont store cloc
                                 case cont of
                                    (TryCatch pos eloc cloc')     -> Just (pos, eloc, cloc')
                                    (App _ _ _ cloc')        -> getFirstTryCatch cloc' store
                                    (Invoke _ _ _ _ _ cloc') -> getFirstTryCatch cloc' store


destrStck :: Box -> Maybe (Value, StckLocation)
destrStck (Stck v loc) = Just (v, loc)
destrStck _            = Nothing



initEnv :: EnvLocation
initEnv = EnvLocation 0


initStack :: StckLocation
initStack = StckLocation 0


initCont :: ContLocation
initCont = ContLocation 0


initStore :: Store
initStore = Store (M.singleton 0 Null) 1



