module Robot.Machine.Step where

import           GHC.Stack

import           Data.Maybe
import qualified Data.Vector as Vector

import           Robot.Machine.Instruction
import           Robot.Machine.Semantics
import           Robot.Machine.Store
import           Robot.Term


type State = (PrgPosition, EnvLocation, StckLocation, Store, ContLocation, [Int], [Int])


step :: Vector.Vector Instruction -> State -> Either State (Value, [Int])
step is state = case instr of
  IUnit           -> Left $ pushval_  VUnit    state
  IFalse          -> Left $ pushval_  VFalse   state
  ITrue           -> Left $ pushval_  VTrue    state
  (IInt i)        -> Left $ pushval_  (VInt i) state
  IPlus           -> Left $ intop_    (+)      state
  IMinus          -> Left $ intop_    (-)      state
  IMul            -> Left $ intop_    (*)      state
  IDiv            -> Left $ intop_    div      state
  IEq             -> Left $ intcmpop_ (==)     state
  INonEq          -> Left $ intcmpop_ (/=)     state
  ILt             -> Left $ intcmpop_ (<)      state
  ILe             -> Left $ intcmpop_ (<=)     state
  IGt             -> Left $ intcmpop_ (>)      state
  IGe             -> Left $ intcmpop_ (>=)     state
  (IJump p)       -> Left $ jump_     p        state
  (IJumpIfTrue p) -> Left $ jumpif_   p        state
  (IVar eo)       -> Left $ pushvar_  eo       state
  (ILambda p)     -> Left $ lambda_   p        state
  (IObject n m p) -> Left $ object_   n m p    state
  (ITryCatch c)   -> Left $ trycatch_ c        state
  IApp            -> Left $ app_               state
  ICallCC         -> Left $ callcc_            state
  (ISet eo)       -> Left $ set_      eo       state
  IPop            -> Left $ pop_               state
  IThrow          -> Left $ throw_             state
  (IInvoke m)     -> Left $ invoke_   m        state
  IRead           -> Left $ read_              state
  IWrite          -> Left $ write_             state
  IReturn         -> ret_ state

  where (pos, _, _, _, _, _, _) = state
        instr = fromMaybe (errorWithStackTrace $ show state) (getInstr is pos)


pushval_ :: Value -> State -> State
pushval_ v (pos, env, stack, store, cont, input, output) = let (stack', store') = pushVal v stack store
                                                           in  (next pos, env, stack', store', cont, input, output)


intop_ :: (Int -> Int -> Int) -> State -> State
intop_ f s@(pos, env, stack, store, cont, input, output) = case intOp f stack store of
                                                             Just (stack', store') -> (next pos, env, stack', store', cont, input, output)
                                                             Nothing               -> showError s


intcmpop_ :: (Int -> Int -> Bool) -> State -> State
intcmpop_ p s@(pos, env, stack, store, cont, input, output) = case intCmpOp p stack store of
                                                                Just (stack', store') -> (next pos, env, stack', store', cont, input, output)
                                                                Nothing               -> showError s


jump_ :: PrgPosOffset -> State -> State
jump_ ppo (pos, env, stack, store, cont, input, output) = (jump ppo pos, env, stack, store, cont, input, output)


jumpif_ :: PrgPosOffset -> State -> State
jumpif_ ppo s@(pos, env, stack, store, cont, input, output) = case jumpIf ppo pos stack store of
                                                                Just (pos', stack') -> (pos', env, stack', store, cont, input, output)
                                                                Nothing             -> showError s


pushvar_ :: EnvOffset -> State -> State
pushvar_ eo s@(pos, env, stack, store, cont, input, output) = case pushVar eo env stack store of
                                                                Just (stack', store') -> (next pos, env, stack', store', cont, input, output)
                                                                Nothing               -> showError s


pop_ :: State -> State
pop_ s@(pos, env, stack, store, cont, input, output) = case pop stack store of
                                                         Just stack' -> (next pos, env, stack', store, cont, input, output)
                                                         Nothing     -> showError s


set_ :: EnvOffset -> State -> State
set_ eo s@(pos, env, stack, store, cont, input, output) = case set eo env stack store of
                                                            Just (stack', store') -> (next pos, env, stack', store', cont, input, output)
                                                            Nothing               -> showError s


lambda_ :: PrgPosition -> State -> State
lambda_ fun (pos, env, stack, store, cont, input, output) = let (stack', store') = lambda fun env stack store
                                                            in  (next pos, env, stack', store', cont, input, output)


object_ :: String -> PrgPosition -> Maybe PrgPosition -> State -> State
object_ mname method parent (pos, env, stack, store, cont, input, output) = let (stack', store') = object mname method parent env stack store
                                                                            in  (next pos, env, stack', store', cont, input, output)


trycatch_ :: PrgPosition -> State -> State
trycatch_ catchpos (pos, env, stack, store, cont, input, output) = let (cont', store') = trycatch catchpos env cont store
                                                                   in  (next pos, env, stack, store', cont', input, output)


throw_ :: State -> State
throw_ s@(_, _, stack, store, cont, input, output) = let (pos', env', stack', store', cont') = fromMaybe (showError s) (throw stack cont store)
                                                     in  (pos', env', stack', store', cont', input, output)


keepIo :: ((PrgPosition, EnvLocation, StckLocation, Store, ContLocation) -> Maybe (PrgPosition, EnvLocation, StckLocation, Store, ContLocation)) -> State -> State
keepIo transform s@(pos, env, stack, store, cont, input, output) = let result = transform (pos, env, stack, store, cont)
                                                                       (pos', env', stack', store', cont') = fromMaybe (showError s) result
                                                                   in  (pos', env', stack', store', cont', input, output)


callcc_ :: State -> State
callcc_ = keepIo callcc


app_ :: State -> State
app_ = keepIo app


invoke_ :: String -> State ->  State
invoke_ methodName = keepIo (invoke methodName)


read_ :: State -> State
read_ (pos, env, stack, store, cont, input, output) = let v = head input
                                                          input' = tail input
                                                          (stack', store') = pushVal (VInt v) stack store
                                                      in  (next pos, env, stack', store', cont, input', output)


write_ :: State -> State
write_ s@(pos, env, stack, store, cont, input, output) = case write stack store of
                                                           Just (stack', store', VInt out) -> (next pos, env, stack', store', cont, input, output ++ [out])
                                                           _                               -> showError s


ret_ :: State -> Either State (Value, [Int])
ret_ s@(_, _, stack, store, cont, input, output) = case (singletonStack stack store, getFirstApp cont store) of
                                      (Just v, Nothing) -> Right (v, output)
                                      (Just v, Just c)  -> Left $ let (pos', env', stack', store', cont') = ret v c store
                                                                  in  (pos', env', stack', store', cont', input, output)
                                      _                 -> showError s



showError :: State -> a
showError s = errorWithStackTrace $ show s
