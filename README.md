# Robot

The Robot project.

This is a stack VM for simple Bool-Int language based on untyped lambda calculus.

The VM is inspired by the SECD and CESK machines.

The input program is first simplified into a *reduced term language*.
This reduced language has functions with only one parameter (currying),
uses *De Bruijn indexes* instead of names for variables,
translates logical operations into lazy `if`s and
compiles away `let`s and `letrec`s into function applications.

Then the *reduced term language* is compiled into bytecode, that the VM executes.

## VM Structure

The VM has this main components:

 * **C**ontrol -- address of current instruction in the bytecode array (program counter)
 * **E**nvironment -- binds variables to values in Store
 * **Sto**re -- map from *store addresses* to values
 * **Sta**ct -- holds intermediate results (the VM is a stack machine)
 * **K**ontinuation -- stack of frames (function application, method invocation or exception handler)
 * **I** / **O** -- list of integers on the input and output


## VM Features

The language/VM supports the following features:

 * based on λ calculus
    * booleans
    * integers
 * anonymous functions (lambdas)
 * mutable variables
 * let-rec
 * currying
 * closures
 * first-class continuations
 * call with current continuation (call/cc)
 * exceptions
 * objects
 * I/O -- read / write commands


## Building

The VM is written in Haskell programming language.
To build the project you will need the following software:

 * GHC >= 7.8
 * cabal-install >= 1.18

You can build the project using the following commands:

```
cabal install --enable-tests --only-dependencies ;
cabal configure --enable-tests ;
cabal build
```

You can also run these commands via the ``build.sh`` script.


## Running

To start the VM, run the command ``cabal run <programfile> <inputfile> <outputfile>``.
The VM expects the program in the file `<programfile>`.

### Input / Output

The language/VM also supports a simple I/O, using the `read` and `write` commands.
The files for input and output are specified on the commandline.
You can also set them as `-`, if you want to use `stdin` or `stdout` respectively.

Example: run the program ``cabal run io/factorials.lambda io/input io/output``.

The program `io/factorials.lambda` reads the size of input and outputs its factorial for every integer on input.
So if the file `io/input` contains ``2 4 5``, the program writes to the file `io/output` ``24 120``.


## Example programs

All example programs are in the file ``tests/Robot/Programs.hs``.
To run all example programs and test if they yield the correct value, run ``cabal test``.

## References

 * SECD -- http://www.brics.dk/RS/03/33/BRICS-RS-03-33.pdf
 * CESK -- http://matt.might.net/articles/cesk-machines/
 * Exceptions in CESK -- http://matt.might.net/articles/oo-cesk/

