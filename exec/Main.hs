#!/usr/bin/env runhaskell

--
-- Copyright (c) 2014 Ondra Pelech
-- GPL version 3 or later (see http://www.gnu.org/copyleft/gpl.html)
--

module Main where

import Control.Monad
import System.Environment

import Robot.Lang
import Robot.Term
import Robot.StaticEvaluator
--import Robot.Interpret.Eval
import Robot.Machine.Instruction
import Robot.Machine.Exec


-- | 'main' runs the main program
main :: IO ()
main =           do putStrLn "Start\n"
                    [programfile, inputfile, outputfile] <- getArgs
                    program <- readFile programfile
                    input <- if inputfile == "-"
                                then getContents
                                else readFile inputfile
                    let lang  = read program
                        slang = langSimpl lang
                        term  = lang2term slang
                        termse = staticeval term
                        inst  = term2ins termse
--                        (val, stor) = evalTerm term
                        inputlist = map read (words input)
                        (val, outputlist) = runio inputlist (translate termse)
                        output = unwords (map show outputlist)
                    putStr "Simp:\t"
                    print slang
                    putStr "Term:\t"
                    print term
                    putStr "TermSE:\t"
                    print termse
                    putStr "Inst:\t"
                    print inst
                    putStr "\n\n\n"
--                     putStr "Eval:\t"
--                     print val
--                     putStr "Stor:\t"
--                     print stor
--                     putStr "\n\n"
                    when (inputfile == "-") (putStrLn "Waiting for stdin\n")
                    putStr "Run:\t"
                    print val
                    putStr "\n\n"
                    if outputfile == "-"
                       then putStrLn output
                       else writeFile outputfile output
